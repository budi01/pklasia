# PKLAsia
## Bagaimana menggunakan direktori ini?
Silakan menggunakan direktorinya untuk menyimpan data laporannya. Jika sudah terbiasa menggunakan git, gunakan prosedur git sehingga versi laporan bisa tersimpan: Clone repository, lalu edit template menjadi laporan masing-masing, dan lakukan commit untuk upload kembali file laporannya ke repository. Dengan demikian kemajuan laporan bisa dimonitor secara kontinyu
